# Awesome Projects List

A curated list of hand picked awesome projects I have come across.

<div align="center">
	<img width="500" height="350" src="logo.svg" alt="Awesome">
	<br>
</div>


Instead of them getting lost in my bookmarks or tabs or just flat out forgetting
what those awesome projects where, I seem to have found a need for keeping track
of them somehow. This is my attempt at trying to maintain one of those awesome
lists.


## Contents

- [Development](#development)
- [Kubernetes](#kubernetes)
- [Privacy](#privacy)
- [Tools](#tools)
- [Android](#android)


---

## Development

- [gitignore](https://www.toptal.com/developers/gitignore) - A `.gitignore` producer
- [localstack](https://github.com/localstack/localstack) - A fully functional local AWS cloud stack
- [cheat.sh](https://cheat.sh/) - A curl-able cheatsheet
- [tldr](https://tldr.sh/) - A tldr cli tool
- [alcritty](https://wiki.archlinux.org/title/Alacritty) - GPU Accellerated Terminal Emulator written in Rust
- [tpm](https://github.com/tmux-plugins/tpm) - TMUX plugin Manager
- [tmux resurrect](https://github.com/tmux-plugins/tmux-resurrect) - TMUX persistent environment save and restart plugin


## Kubernetes

- [kubestack](https://www.kubestack.com/) - Kubernetes export to Terraform tool for free
- [kubeletmein](https://github.com/4ARMED/kubeletmein) - Security testing tool for Kubernetes, abusing kubelet credentials on public cloud providers
- [kubescape](https://github.com/armosec/kubescape) - A K8s open-source security and risk analysis tool


## Privacy

- [simplelogin](https://simplelogin.io/) - Email alias service
- [skiff](https://www.skiff.org/) - Private & decentrailized workspace
- [safing.io](https://safing.io/) - Fighting surveillance
- [ipleak](https://ipleak.net/) - IP Leak Tester
- [cover your tracks](https://coveryourtracks.eff.org/) - See how trackers view your browser
- [dns leak test](https://dnsleaktest.com/) - Check if your DNS is leaking
- [browser leaks](https://browserleaks.com/) - Check what in your browser is leaking
- [webbkoll](https://webbkoll.dataskydd.net/en) - Check what data protecting measures your site has
- [privacy score](https://privacyscore.org/) - Check your websites privacy score
- [mozilla's observatory](https://observatory.mozilla.org/) - Mozialla's website privacy tool
- [firefox monitor](https://monitor.firefox.com/) - See if you have been part of a privacy breach
- [dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy) - A flexible DNS proxy with encryption
- [testssl](https://github.com/drwetter/testssl.sh) Testing your websites TLS/SSL encryption
- [arch-delubevpn](https://github.com/binhex/arch-delugevpn) - A arch based docker container with deluge and openvpn
- [element im](https://element.io/) - Matrix messaging frontend
- [ungoogled-chromium](https://github.com/Eloston/ungoogled-chromium) - UnGoogled Chrome Browser
- [pass](https://www.passwordstore.org/) - unix password manager
- [DeleteMe](https://joindeleteme.com/) - Remove your personal data from Google
- [Masking](https://dnt.abine.com/#feature/masking) - Mask your personal data in online forms
- [searx](https://searx.space/) - Privacy minded FOSS Self Hosted search engine
- [twake](https://github.com/linagora/Twake) - MS Teams replacement and more, FOSS and self hosted


## Tools

- [radicale](https://radicale.org/v3.html) - FOSS CalDAV and CardDAV Server
- [ultralist](https://github.com/ultralist/ultralist) - Simple task management tool for techies
- [calibre](https://calibre-ebook.com/) - E-Book reader
- [LibreOffice](https://www.libreoffice.org/) - MS Office Alternative
- [LuLu](https://github.com/objective-see/LuLu) - Free macOS firewall
- [Wireshark](https://www.wireshark.org/) - Network Analyzer
- [flameshot](https://flameshot.org/) - Screenshot tool
- [tutanota](https://tutanota.com/) - Secure Email
- [rofi](https://github.com/davatorium/rofi) - Application Launcher and dmenu replacement
- [wttr.in](https://github.com/chubin/wttr.in) - Weather in your terminal
- [Rotki](https://rotki.com/) - FOSS Privacy Minded Self-service Crypto Portfolio Manager Platform
- [terminal.sexy](https://terminal.sexy/) - Lets you make a terminal or vim theme and export in many formats including st and alacritty


## Android

- [vanced](https://vancedapp.com/) - Youtube Android app without ads
- [slide](https://github.com/Haptic-Apps/Slide) - Ad Free FOSS Reddit App for Android
- [magisk](https://github.com/topjohnwu/Magisk) - Root for Android
- [lineageos](https://www.lineageos.org/) - Google Free Android OS for most android phones out there
- [F-Droid](https://f-droid.org/) - Alternative Google-less FOSS play store
- [aurora store](https://f-droid.org/en/packages/com.aurora.store/) - Privacy minded Google Play Store Alternative
- [AnySoftKeyboard](https://f-droid.org/en/packages/com.menny.android.anysoftkeyboard/) - FOSS Privacy Minded Android Keyboard
- [OpenBoard](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/) - FOSS Privacy Minded Android Keyboard
- [Simple Keyboard](https://f-droid.org/en/packages/com.simplemobiletools.keyboard/) - FOSS Privacy Minded Android Keyboard

